import Vue from "vue";
import Router from "vue-router";
import OverviewGroups from "./components/OverviewGroups.vue";
import CreateGroup from "./components/CreateGroup.vue";
import Admin from "@/Admin.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Admin",
      component: Admin
    },
    {
      path: "/overviewgroups",
      name: "OverviewGroups",
      component: OverviewGroups
    },
    {
      path: "/creategroup",
      name: "CreateGroup",
      component: CreateGroup
    }
  ]
});
